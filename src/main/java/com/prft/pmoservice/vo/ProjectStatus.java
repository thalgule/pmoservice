package com.prft.pmoservice.vo;

public class ProjectStatus {

	private Integer status;
	private Integer status_count;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus_count() {
		return status_count;
	}

	public void setStatus_count(Integer status_count) {
		this.status_count = status_count;
	}

}
