package com.prft.pmoservice.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prft.pmoservice.entity.Project;
import com.prft.pmoservice.service.ProjectService;
import com.prft.pmoservice.vo.ProjectStatus;

@RestController
@RequestMapping("/api/project")
public class ProjectController {
	@Autowired
	private ProjectService projectService;

	@GetMapping(value = "", produces = { "application/json" })
	public ResponseEntity<List<Project>> getProjects() {
		List<Project> projectList = new ArrayList<Project>();
		projectList = this.projectService.fetchAllProjects();
		return new ResponseEntity<List<Project>>(projectList, HttpStatus.OK);
	}

	@GetMapping(value = "/active-projects", produces = { "application/json" })
	public ResponseEntity<List<Project>> getActiveProjects() {
		List<Project> projectList = projectService.fetchActiveProjects();
		return new ResponseEntity<List<Project>>(projectList, HttpStatus.OK);
	}

	@PostMapping(value = "", consumes = { "application/json" }, produces = { "application/json" })
	public ResponseEntity<Map<String, Long>> createProject(@RequestBody Project project) {
		Long projectId = projectService.createProject(project);
		Map<String, Long> response = new HashMap<String, Long>();
		response.put("ProjectId", project.getProjectId());
		return new ResponseEntity<Map<String, Long>>(response, HttpStatus.OK);
	}

	@GetMapping(value = "/status", produces = { "application/json" })
	public ResponseEntity<Map<String, List<ProjectStatus>>> getProjectStatus() {
		List<ProjectStatus> projectStatusList = projectService.getProjectStatus();
		Map<String, List<ProjectStatus>> response = new HashMap<String, List<ProjectStatus>>();
		response.put("records", projectStatusList);
		ResponseEntity<Map<String, List<ProjectStatus>>> responseEntity = new ResponseEntity<Map<String, List<ProjectStatus>>>(
				response, HttpStatus.OK);
		return responseEntity;
	}

}
