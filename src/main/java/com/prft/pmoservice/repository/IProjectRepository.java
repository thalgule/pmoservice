package com.prft.pmoservice.repository;

import java.util.List;

import com.prft.pmoservice.vo.ProjectStatus;

public interface IProjectRepository {

	public List<ProjectStatus> getProjectStatus();
}
