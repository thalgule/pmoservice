package com.prft.pmoservice.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prft.pmoservice.vo.ProjectStatus;

@Repository
public class ProjectRepositoryImpl implements IProjectRepository{
	@Autowired
	private EntityManager entityManager;
	
	private Session getSession() {
		return entityManager.unwrap(Session.class);
	}

	@Override
	public List<ProjectStatus> getProjectStatus() {
		List<ProjectStatus> projectStatusList = new ArrayList<ProjectStatus>();
		try {
			Session session = getSession();
			Query query = session.createSQLQuery("SELECT status, count(status) AS status_count FROM PROJECT group by status");
			List list = query.list();
			if(list != null && !list.isEmpty()) {
				for (Object object : list) {
					Object[] result = (Object[]) object;
					ProjectStatus projectStatus = new ProjectStatus();
					projectStatus.setStatus(Integer.parseInt(result[0].toString()));
					projectStatus.setStatus_count(Integer.parseInt(result[1].toString()));
					projectStatusList.add(projectStatus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectStatusList;
	}

}
