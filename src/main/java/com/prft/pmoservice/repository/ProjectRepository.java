package com.prft.pmoservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prft.pmoservice.entity.Project;

public interface ProjectRepository extends JpaRepository<Project, Long>, IProjectRepository {

}
