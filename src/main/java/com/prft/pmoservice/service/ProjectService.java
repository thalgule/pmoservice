package com.prft.pmoservice.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.prft.pmoservice.entity.Project;
import com.prft.pmoservice.repository.ProjectRepository;
import com.prft.pmoservice.vo.ProjectStatus;

@Service
public class ProjectService {
	@Autowired
	private ProjectRepository projectRepository;

	public List<Project> fetchAllProjects() {
		return projectRepository.findAll();
	}

	public List<Project> fetchActiveProjects() {
		List<Project> projectList = fetchActiveProjects();
		if (projectList != null & !projectList.isEmpty()) {
			List<Project> activeProjectList = projectList.stream().filter(project -> project.getStatus() == 1)
					.collect(Collectors.toList());
			return activeProjectList;
		} else {
			return projectList;
		}
	}

	public Long createProject(Project project) {
		project.setStartDate(new Timestamp(new Date().getTime()));
		project.setUpdateDate(new Timestamp(new Date().getTime()));
		if (StringUtils.isEmpty(project.getProjectDesc())) {
			project.setProjectDesc(project.getProjectName());
		}
		projectRepository.saveAndFlush(project);
		return project.getProjectId();
	}

	public List<ProjectStatus> getProjectStatus() {
		List<ProjectStatus> projectStatusList = new ArrayList<ProjectStatus>();
		try {
			projectStatusList = projectRepository.getProjectStatus();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectStatusList;
	}

}
