package com.prft.pmoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableAutoConfiguration
@ComponentScan("com.prft.pmoservice")
@EntityScan("com.prft.pmoservice.entity")
@EnableJpaRepositories("com.prft.pmoservice.repository")
@SpringBootApplication
public class PMOServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PMOServiceApplication.class, args);
	}

}
