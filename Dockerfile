FROM openjdk:11
VOLUME /tmp

EXPOSE 8089

COPY target/*.jar /api/app.jar

RUN ls /api

WORKDIR /api

ENTRYPOINT exec java -Dspring.profiles.active=${SPRING_PROFILE} -Xms64m -Xmx256m -jar app.jar
